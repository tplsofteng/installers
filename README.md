# installers
Collection of utility scripts to setup various software

## Install VSCode/codium plugins
```script
./vscode/install.sh vscode/plugins.txt
```
## Install Intellij plugins
```script
./idea/install.sh idea/plugins.txt
```
