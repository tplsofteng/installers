#!/bin/env bash
[ -z "$1" ] && echo "use: install.sh <plugin list text file>" && exit 1

command -v code &> /dev/null && CODE_CMD="code" 
command -v vscodium &> /dev/null && CODE_CMD="vscodium" 
echo "$CODE_CMD"

# installs plugins from default plugin repo
install_plugin() {
    echo "install_plugin $1"
    "$CODE_CMD" --install-extension "$1"
}

# Read plugin IDs from the file provided as an argument and install each plugin
filename="$1"
while IFS= read -r line || [ -n "$line" ]; do
    
    # Remove comments at the end of the line
    plugin_id=`echo "$line" | sed 's/\s*#.*//g'`

    # Skip empty lines
    [ -z "$plugin_id" ] && continue
    
    install_plugin "$plugin_id"
done < "$filename"
