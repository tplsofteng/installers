#!/bin/sh
[ -z "$1" ] && echo "use: install.sh <plugin list text file>" && exit 1

if ps ax | grep -v grep | grep "com.intellij.idea.Main" > /dev/null; then
    echo "Please close all running intellij instances before installing plugins"
    exit 1
fi

[ -d /usr/share/idea ] && DETECTED_IDEA_DIR="/usr/share/idea"
[ -d /snap/intellij-idea-community/current ] && DETECTED_IDEA_DIR="/snap/intellij-idea-community/current"

IDEA_DIR="${IDEA_DIR:-"$DETECTED_IDEA_DIR"}"
IDEA_CLI="$IDEA_DIR/bin/idea.sh"


# installs plugins from default plugin repo
install_plugin() {
    echo "install_plugin $1"
    "$IDEA_CLI" installPlugins "$1"
}

# install plugins from specific plugin repo
install_external_plugin() {
    echo "install_external_plugin $1 from $2"
    "$IDEA_CLI" installPlugins "$1" "$2"
}

install () {
    echo "Installing into $IDEA_DIR"
    # Read plugin IDs from the file provided as an argument and install each plugin
    filename="$1"
    while IFS= read -r line || [ -n "$line" ]; do
    
        # Remove comments at the end of the line
        plugin_id=`echo "$line" | sed 's/\s*#.*//g'`

        # Skip empty lines
        [ -z "$plugin_id" ] && continue
    
        # uncomment to use a specfic plugin repo
        #PLUGIN_REPO_URL="https://plugins.jetbrains.com/plugin/download?pluginId=$PLUGIN_ID"
        #install_external_plugin "$plugin_id" "$PLUGIN_REPO_URL"
    
        # comment out and use above for custom repos
        install_plugin "$plugin_id"
    done < "$filename"
}

install "$1"
